package one.mcpvp.raid;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import one.mcpvp.raid.commons.Config;
import one.mcpvp.raid.database.Database;
import one.mcpvp.raid.database.Tables;
import one.mcpvp.raid.player.PlayerManager;

public class RaidPlugin extends JavaPlugin
{
	@Override
	public void onLoad()
	{
		
	}
	
	@Override
	public void onEnable()
	{
		//Create database connection pool
		Config databaseConfig = new Config("database", this)
				{
					@Override
					public void onCreate(FileConfiguration configFile)
					{
						//This will only be called on the first time the file is created
						//aka it's the defaults
						configFile.set("database.ip", "localhost");
						configFile.set("database.port", "101");
						configFile.set("database.username", "localhost");
						configFile.set("database.password", "localhost");
					}
				};
		FileConfiguration config = databaseConfig.getConfig();
		String ip = config.getString("database.ip");
		String port = config.getString("database.port");
		String databaseName = config.getString("database.database");
		String username = config.getString("database.username");
		String password = config.getString("database.password");
		Database database = new Database(ip, port, databaseName, username, password);
		
		Tables tables = new Tables(database);
		tables.generateTableIfNotExits();
		
		
		PlayerManager playerManager = new PlayerManager(this);
	}
	
	@Override
	public void onDisable()
	{
		
	}
	
	/**
	 * Java plugin istance used for bukkit paramters
	 * @return JavaPlugin instance
	 */
	public static RaidPlugin getInstance()
	{
		return JavaPlugin.getPlugin(RaidPlugin.class);
	}
}
