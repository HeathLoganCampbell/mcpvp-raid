package one.mcpvp.raid;

import one.mcpvp.raid.commons.CC;

public class Raid 
{
	public static String NOTIFY_TEAM_MEMBER_JOIN = CC.yellow + "TEAM] %s has joined.",
						 NOTIFY_TEAM_MEMBER_LEFT = CC.yellow + "TEAM] %s has left.";
}
