package one.mcpvp.raid.player;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Bukkit;

import lombok.Getter;
import one.mcpvp.raid.RaidPlugin;
import one.mcpvp.raid.commons.Console;

/**
 *  <h1>RaidPlayer Manager</h1>
 *  <p>
 *  Keeps track of our raid data per player
 *  RaidPlayer is added on login and removed
 *  on disconnect of the player at 
 *  {@link one.mcpvp.raid.player.PlayerListener PlayerListener}
 *  </p>
 *  <p>
 *  Note, All Online players should have a raid Player entry, it can be hidden if need be {@link one.mcpvp.raid.player.RaidPlayer#setVisable SetVisability}.
 *  </p>
 */
public class PlayerManager 
{
	@Getter
	private HashMap<UUID, RaidPlayer> onlinePlayers;
	
	public PlayerManager(RaidPlugin raidPlugin)
	{
		this.onlinePlayers = new HashMap<>();
		
		//Register listeners
		Bukkit.getPluginManager().registerEvents(new PlayerListener(this), raidPlugin);
	}
	
	
	public RaidPlayer registerPlayer(String username, UUID uniqueId)
	{
		RaidPlayer raidPlayer = new RaidPlayer(username, uniqueId);
		this.registerPlayer(raidPlayer);
		return raidPlayer;
	}
	
	public RaidPlayer getPlayer(UUID uniqueId)
	{
		return this.getOnlinePlayers().get(uniqueId);
	}
	
	public void unregisterPlayer(UUID uniqueId)
	{
		RaidPlayer raidPlayer = this.getPlayer(uniqueId);
		
		this.getOnlinePlayers().remove(uniqueId);
		Console.log("PlayerManager", raidPlayer.getUsername() + " has been unregistered.");
	}
	
	/**
	 * <p>
	 * Used to register the raidplayer so the server can use the data
	 * </p>
	 * <p>
	 * Note, All Online players should have a raid Player entry
	 * </p>
	 * @param raidPlayer
	 * @return raidPlayer
	 */
	public RaidPlayer registerPlayer(RaidPlayer raidPlayer)
	{
		this.getOnlinePlayers().put(raidPlayer.getUniqueId(), raidPlayer);
		
		Console.log("PlayerManager", raidPlayer.getUsername() + " has been registered.");
		return raidPlayer;
	}
}
