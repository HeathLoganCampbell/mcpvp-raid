package one.mcpvp.raid.player.events;

import lombok.NonNull;
import one.mcpvp.raid.player.RaidPlayer;

public class RaidPlayerLoadEvent extends RaidPlayeEvent
{
	public RaidPlayerLoadEvent(@NonNull RaidPlayer raidPlayer) 
	{
		super(raidPlayer);
	}
}
