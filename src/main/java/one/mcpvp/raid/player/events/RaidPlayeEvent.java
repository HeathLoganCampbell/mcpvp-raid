package one.mcpvp.raid.player.events;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import one.mcpvp.raid.commons.EventCancellable;
import one.mcpvp.raid.player.RaidPlayer;

@RequiredArgsConstructor
public class RaidPlayeEvent extends EventCancellable
{
	@Getter @NonNull
	private RaidPlayer raidPlayer;
}
