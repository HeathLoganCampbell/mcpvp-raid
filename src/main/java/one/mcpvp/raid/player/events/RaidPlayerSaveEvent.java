package one.mcpvp.raid.player.events;

import lombok.NonNull;
import one.mcpvp.raid.player.RaidPlayer;

public class RaidPlayerSaveEvent extends RaidPlayeEvent
{
	public RaidPlayerSaveEvent(@NonNull RaidPlayer raidPlayer) 
	{
		super(raidPlayer);
	}
}
