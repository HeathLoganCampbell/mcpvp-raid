package one.mcpvp.raid.player;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class PlayerListener implements Listener
{
	@Getter @NonNull
	private PlayerManager playerManager;
	
	public void join(String username, UUID uniqueId)
	{
		this.getPlayerManager().registerPlayer(username, uniqueId);
	}
	
	public void leave(Player player)
	{
		UUID uniqueId = player.getUniqueId();
		this.getPlayerManager().unregisterPlayer(uniqueId);
	}
	
	//
	//  LISTENERS
	//
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void onPreLogin(AsyncPlayerPreLoginEvent e)
	{
		UUID uniqueId = e.getUniqueId();
		String username = e.getName();
		this.join(username, uniqueId);
	}

	@EventHandler
	public void onKick(PlayerKickEvent e)
	{
		Player player = e.getPlayer();
		this.leave(player);
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e)
	{
		Player player = e.getPlayer();
		this.leave(player);
	}
}
