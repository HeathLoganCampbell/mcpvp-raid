package one.mcpvp.raid.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import one.mcpvp.raid.RaidPlugin;
import one.mcpvp.raid.database.Database;

@AllArgsConstructor
@Getter(AccessLevel.PROTECTED)
public class PlayerDatabase 
{
	public static final String FETCH_RAIDPLAYER = "SELECT * FROM `RaidPlayers` WHERE `uniquie_id`=?";
	
	private Database database;
	private RaidPlugin plugin;
	
	public void loadRaidPlayer(RaidPlayer raidPlayer)
	{
		Bukkit.getServer().getScheduler().runTaskAsynchronously(this.plugin, () ->
		{
			Connection connection = null;
			PreparedStatement statement = null;
			ResultSet resultSet = null;
			try
			{
				connection = this.getDatabase().getHikari().getConnection();
	            
	            statement = connection.prepareStatement(FETCH_RAIDPLAYER);
	            statement.setString(1, raidPlayer.getUniqueId().toString());//uniquie_id
	            
	            resultSet = statement.executeQuery(FETCH_RAIDPLAYER);
	            raidPlayer.setLoaded(true);
	        } 
			catch (SQLException e) 
			{
	            e.printStackTrace();
	        } finally 
			{
	        	try 
	        	{
					statement.close();
					connection.close();
				}
	        	catch (SQLException e) 
	        	{
					e.printStackTrace();
				}
	        }
		});
	}
}
