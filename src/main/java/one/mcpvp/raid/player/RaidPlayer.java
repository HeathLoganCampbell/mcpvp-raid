package one.mcpvp.raid.player;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.Getter;
import lombok.Setter;
import one.mcpvp.raid.team.RaidTeam;

/**
 * All data related to the player
 * for this plugin (eg teams, warps)
 * 
 */
public class RaidPlayer 
{
	public static final RaidPlayer NULL_RAIDPLAYER = new RaidPlayer("NULL PLAYER", UUID.randomUUID());
	
	@Getter @Setter
	private String username;
	@Getter @Setter
	private UUID uniqueId;
	@Getter @Setter
	private boolean loaded = false;
	
	/**
	 * Will trigger {@link one.mcpvp.raid.player.events.RaidPlayerLoadEvent RaidPlayerLoadEvent}
	 * when finished loading
	 */
	@Getter
	private boolean triggerLoadEvent = true;
	/**
	 * Hides non admin players from seeing this data
	 */
	@Getter @Setter
	private boolean visable = false;
	
	@Getter @Setter
	private int teamId;
	
	@Getter @Setter
	private RaidTeam team;
	
	public RaidPlayer(String username, UUID uniqueId)
	{
		this(username, uniqueId, true);
	}
	
	public RaidPlayer(String username, UUID uniqueId, boolean triggerLoadEvent)
	{
		this.username = username;
		this.uniqueId = uniqueId;
		
		this.triggerLoadEvent = triggerLoadEvent;
	}
	
	public Player getBukkitPlayer()
	{
		return Bukkit.getPlayer(this.getUniqueId());
	}
}
