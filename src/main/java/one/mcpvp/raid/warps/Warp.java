package one.mcpvp.raid.warps;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@RequiredArgsConstructor
public class Warp 
{
	@Getter(AccessLevel.PRIVATE)
	private Location cacheLocation;
	
	@Getter @Setter @NonNull
	private Integer warpId;
	@Getter @Setter @NonNull
	private String worldName;
	@Getter @Setter @NonNull
	private Double x, y, z;
	
	/**
	 * To bukkit location
	 * @return location
	 */
	public Location toLocation()
	{
		if(this.getCacheLocation() == null)
			this.cacheLocation = new Location(Bukkit.getWorld(getWorldName())
												, this.getX()
												, this.getY()
												, this.getZ()
											);
		return this.cacheLocation;
	}
	
}
