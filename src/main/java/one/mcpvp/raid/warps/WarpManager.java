package one.mcpvp.raid.warps;

import java.util.HashMap;

import lombok.Getter;

public class WarpManager 
{
	@Getter
	private HashMap<Integer, Warp> warps;
	
	public WarpManager()
	{
		this.warps = new HashMap<>();
	}
	
	
}
