package one.mcpvp.raid.team;

import java.util.HashMap;

import lombok.Getter;

public class TeamManager 
{
	@Getter
	private HashMap<Integer, RaidTeam> onlineTeams;
	
	public TeamManager()
	{
		this.onlineTeams = new HashMap<>();
	}
	
	public RaidTeam getOnlineTeam(int teamId)
	{
		return this.getOnlineTeams().get(teamId);
	}
}
