package one.mcpvp.raid.team.memebers;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum TeamRole 
{
	LEADER("L"),
	MODERATOR("M"),
	NORMAL("");
	
	@Getter @NonNull
	private String symbol;
}
