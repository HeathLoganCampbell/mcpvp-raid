package one.mcpvp.raid.team.memebers;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import one.mcpvp.raid.player.PlayerManager;
import one.mcpvp.raid.player.RaidPlayer;

@AllArgsConstructor
@Getter @Setter
public class TeamMember
{
	@Getter(AccessLevel.PRIVATE)
	private PlayerManager playerManager;
	
	private int raidPlayerId;
	private String username;
	private UUID uniquieId;
	
	private TeamRole role;
	
	
	
	public boolean isOnline()
	{
		return this.getPlayerManager().getPlayer(this.getUniquieId()) != null;
	}
	
	/**
	 * Get the raidplayer if the player is online,
	 * if the player isn't online, null will be returned.
	 * @return RaidPlayer
	 */
	public RaidPlayer getRaidPlayer()
	{
		return this.getPlayerManager().getPlayer(this.getUniquieId());
	}
	
	public Player getBukkitPlayer()
	{
		return Bukkit.getPlayer(this.getUniquieId());
	}
}
