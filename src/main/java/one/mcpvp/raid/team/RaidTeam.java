package one.mcpvp.raid.team;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import one.mcpvp.raid.team.memebers.TeamMember;


@Getter @Setter
@AllArgsConstructor
public class RaidTeam
{
	private int teamId;
	
	private String name;
	/**
	 * Chat tags of the team that will come up on chat
	 */
	private String tag;
	
	private Location rallyWarp;
	private Location hqWarp;
	
	private int maxPlayerCount;
	
	@Setter(AccessLevel.NONE)
	private List<TeamMember> members; 
	
	public RaidTeam(int teamId, String name, String tag, Location rallyWarp, Location hqWarp, int maxPlayerCount)
	{
		this(
				teamId, 
				name, 
				tag, 
				rallyWarp, 
				hqWarp, 
				maxPlayerCount, 
  /* Members */ new ArrayList<>()
			);
	}
	
	/**
	 * @return number of online players
	 */
	public long getOnlineCount()
	{
		return this.members.stream()
						   .map(TeamMember::getBukkitPlayer)
						   .filter(player -> player != null)
						   .count();
	}
	
	public void sendMessage(String message)
	{
		this.forAllOnlineTeamMembers(player -> player.sendMessage(message));
	}

	/**
	 * <p>
	 * Runs a function all all team members online.
	 * </p>
	 * <code>
	 * this.forAllOnlineTeamMembers(player -> player.sendMessage("This is a message for the whole team"));
	 * </code>
	 * 
	 * 
	 * @param functionPlayer, a player consumer
	 */
	public void forAllOnlineTeamMembers(Consumer<? super Player> functionPlayer)
	{
		this.members.stream()
					.map(TeamMember::getBukkitPlayer)
					.filter(player -> player != null)
					.forEach(functionPlayer);
	}
}
