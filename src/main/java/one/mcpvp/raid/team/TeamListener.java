package one.mcpvp.raid.team;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import one.mcpvp.raid.Raid;
import one.mcpvp.raid.player.RaidPlayer;
import one.mcpvp.raid.player.events.RaidPlayerLoadEvent;

@RequiredArgsConstructor
public class TeamListener implements Listener
{
	@Getter @NonNull
	private TeamManager teamManager;
	
	/**
	 * <p>
	 * Triggered on event RaidPlayerLoadEvent
	 * </p>
	 * 
	 * <p>
	 * If the player logins in but his team hasn't been loaded, we'll load it
	 * </p>
	 * 
	 * 
	 */
	@EventHandler
	public void onRaidPlayerLoad(RaidPlayerLoadEvent e)
	{
		RaidPlayer raidPlayer = e.getRaidPlayer();
		int playersTeamId = raidPlayer.getTeamId();
		
		RaidTeam raidTeam = this.getTeamManager().getOnlineTeam(playersTeamId);
		
		if(raidTeam == null)
		{
			//we need to load in the team
			
			
		} else 
			//a team member is already logged in, so we'll keep it loaded
			raidTeam.sendMessage(String.format(Raid.NOTIFY_TEAM_MEMBER_JOIN, raidPlayer.getUsername()));
	}
}
