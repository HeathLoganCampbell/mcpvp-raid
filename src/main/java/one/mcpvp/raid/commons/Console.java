package one.mcpvp.raid.commons;

import java.util.HashSet;

public class Console 
{
	private static HashSet<String> ALLOW_HEADERS = new HashSet<>();
	private static final String STRING_FORMATE = "%s ]> %s";//Eg PlayerManager ]> Sprock has been registered.
	
	public static void log(String header, String message)
	{
		if(!ALLOW_HEADERS.contains(header)) return;
		String finalMsg = String.format(STRING_FORMATE, header, message);
		System.out.println(finalMsg);
	}
}
