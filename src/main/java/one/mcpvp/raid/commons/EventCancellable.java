package one.mcpvp.raid.commons;

import org.bukkit.event.Cancellable;

public class EventCancellable extends EventBase implements Cancellable
{
	private boolean cancelled = false;
	
	@Override
	public boolean isCancelled() 
	{
		return cancelled;
	}

	@Override
	public void setCancelled(boolean isCancelled)
	{
		this.cancelled = isCancelled;
	}

}
