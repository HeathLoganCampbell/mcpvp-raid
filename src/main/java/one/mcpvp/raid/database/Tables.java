package one.mcpvp.raid.database;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Tables 
{
	public static final String CREATE_RAIDPLAYERS_TABLE = 
			"CREATE TABLE IF NOT EXISTS `RaidPlayers` (\n" + 
			"  `raidplayer_id` INT,\n" + 
			"  `username` VARCHAR(17),\n" + 
			"  `username_lowwercase` VARCHAR(17),\n" + 
			"  `uniquie_id` VARCHAR(32),\n" + 
			"  `personal_warp_id` INT,\n" + 
			"  `coins` INT,\n" + 
			"  `join_timestamp` TIMESTAMP,\n" + 
			"  PRIMARY KEY (`raidplayer_id`),\n" + 
			");";

	public static final String CREATE_RAIDTEAMS_TABLE = 
			"CREATE TABLE IF NOT EXISTS `RaidTeams` (\n" + 
			"  `team_id` INT,\n" + 
			"  `name` VARCHAR(32),\n" + 
			"  `tag` VARCHAR(10),\n" + 
			"  `rally_warp_id` INT,\n" + 
			"  `hq_warp_id` INT,\n" + 
			"  `max_members` INT,\n" + 
			"  `donator_rank` VARCHAR(17),\n" + 
			"  PRIMARY KEY (`team_id`),\n" + 
			");";
	
	public static final String CREATE_TEAMMEMBERS_TABLE = 
			"CREATE TABLE IF NOT EXISTS `TeamMembers` (\n" + 
			"  `raidplayer_id` INT,\n" + 
			"  `team_id` INT,\n" + 
			"  `team_role` VARCHAR(17),\n" + 
			"  `is_active` TINYINT,\n" + 
			"  `join_timestamp` TIMESTAMP,\n" + 
			"  PRIMARY KEY (`raidplayer_id`)\n" + 
			");";
	
	public static final String CREATE_WARPS_TABLE = 
			"CREATE TABLE IF NOT EXISTS `Warps` (\n" + 
			"  `warp_id` INT,\n" + 
			"  `world_name` VARCHAR(50),\n" + 
			"  `x` INT,\n" + 
			"  `y` INT,\n" + 
			"  `z` INT,\n" + 
			"  `creation_timestamp` TIMESTAMP,\n" + 
			"  PRIMARY KEY (`warp_id`)\n" + 
			");";
	
	@Getter @NonNull
	private Database database;
	
	public void generateTableIfNotExits()
	{
		
		//TODO
		Stream.of(
				CREATE_RAIDPLAYERS_TABLE, 
				CREATE_RAIDTEAMS_TABLE,
				CREATE_TEAMMEMBERS_TABLE,
				CREATE_WARPS_TABLE
				).forEach(table ->
				{
					try(
						Connection connection = this.getDatabase().getHikari().getConnection();
			            Statement statement = connection.createStatement();
					   )
					{
			            statement.executeUpdate(table);
			        } catch (SQLException e) {
			            e.printStackTrace();
			        }
				});
	}
}
