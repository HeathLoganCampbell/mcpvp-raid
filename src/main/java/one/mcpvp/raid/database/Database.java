package one.mcpvp.raid.database;

import com.zaxxer.hikari.HikariDataSource;

import lombok.Getter;

public class Database
{
	@Getter
	private HikariDataSource hikari;
	
	public Database(String ip, String port, String database, String username, String password)
	{
		hikari = new HikariDataSource();
        hikari.setDataSourceClassName("com.mysql.jdbc.jdbc2.optional.MysqlDataSource");
        hikari.addDataSourceProperty("serverName", ip);
        hikari.addDataSourceProperty("port", port);
        hikari.addDataSourceProperty("databaseName", database);
        hikari.addDataSourceProperty("user", username);
        hikari.addDataSourceProperty("password", password);
	}
	
	public void destroy()
	{
		if(hikari != null)
			hikari.close();
	}
}
